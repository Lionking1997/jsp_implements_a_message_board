/*
Navicat MySQL Data Transfer

Source Server         : localdb
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : notebook

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-04-11 18:02:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `answer`
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `author` varchar(30) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('1', '19', 'admin', '收到', '2018-03-20 18:43:52');
INSERT INTO `answer` VALUES ('2', '20', 'admin', '实打实啊飒飒飒飒是', '2018-03-23 07:55:29');
INSERT INTO `answer` VALUES ('3', '18', 'admin', '阿萨设计师', '2018-03-26 08:57:20');
INSERT INTO `answer` VALUES ('4', '19', 'admin', '大家准时上交作业，过时不候', '2018-03-26 15:38:48');
INSERT INTO `answer` VALUES ('5', '18', 'admin', '已经交齐', '2018-03-28 21:15:09');
INSERT INTO `answer` VALUES ('6', '18', 'admin', 'asaads时代大厦', '2018-03-29 15:32:04');

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  `author` varchar(30) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('18', '交作业', '学习委员', '2018-03-20 15:40:34', '请明天大家准时交作业，需要提交的作业有jsp，html，js\r\n                                ');
INSERT INTO `message` VALUES ('19', '作业要交啦', '学习委员', '2018-03-23 07:54:39', '注意啦，jsp作业提交完成dsds\r\n                                ');
INSERT INTO `message` VALUES ('20', '阿萨', '学习委员', '2018-03-20 09:06:21', '我们是新时代的青年');
INSERT INTO `message` VALUES ('21', '招聘', '阿里巴巴', '2018-03-20 09:42:53', '阿里巴巴春季招聘将于下月举行，有意愿的小伙伴赶紧报名来吧。');
INSERT INTO `message` VALUES ('22', '两会', '央视记者', '2018-03-22 08:40:57', '热烈祝贺两会在我国首都北京胜利召开。');
INSERT INTO `message` VALUES ('23', '然后就去远行', '李敖', '2018-03-22 08:44:21', '花开可要欣赏，然后就去远行。 \r\n唯有不等花谢，才能记得花红。 \r\n有酒可要满饮，然后就去远行。 \r\n唯有不等大醉，才能觉得微醺。 \r\n有情可要恋爱，然后就去远行。 \r\n唯有恋得短暂，才能爱得永恒。\r\n');
INSERT INTO `message` VALUES ('24', '《自卑与超越》', '阿德勒', '2018-03-22 08:45:24', '是骄傲、虚荣、嫉妒和抱负，支撑你走到今天，你的成长依赖这些负能量，而非天生的善良');
INSERT INTO `message` VALUES ('25', '课程设计', '学习委员', '2018-03-22 19:10:40', '下周进行jsp课程设计，请大家提前做好准备，完成实验要求');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `role` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '202cb962ac59075b964b07152d234b70', '超级管理员');
INSERT INTO `user` VALUES ('2', 'aaa', '202cb962ac59075b964b07152d234b70', '普通管理员');
INSERT INTO `user` VALUES ('3', 'bbb', '202cb962ac59075b964b07152d234b70', '普通管理员');
